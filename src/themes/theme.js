import { createMuiTheme } from '@material-ui/core/styles';
import { DARK_BLUE, SECONDARY_BLUE } from '../const/styleConst';

export const theme = createMuiTheme({
  spacing: 4,
  palette: {
    primary: {
      main: DARK_BLUE,
    },
    secondary: {
      main: SECONDARY_BLUE,
    },
  },
  breakpoints: {
    values: {
      sm: 700,
    },
  },
  overrides: {
    MuiButton: {
      root: {
        width: '100%',
        padding: '10px 16px',
        fontSize: '16px',
        fontWeight: 'bold',
      },
      contained: {
        '&.MuiButton-containedSecondary': {
          '&:hover': {
            opacity: 0,
          },
        },
      },
    },
    MuiPaper: {
      rounded: {
        borderRadius: '8px',
      },
    },
  },
});
