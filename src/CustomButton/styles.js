import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
  error: {
    backgroundColor: theme.palette.error.main,
  },
}));
