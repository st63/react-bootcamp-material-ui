import React from 'react';
import { Button } from '@material-ui/core';
import { useStyles } from './styles';

export const CustomButton = ({ error }) => {
  const classes = useStyles();

  return (
    <Button className={error && classes.error} variant="contained">
      Error
    </Button>
  );
};
