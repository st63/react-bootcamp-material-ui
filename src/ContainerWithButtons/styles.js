import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
  root: {
    width: theme.spacing(100),
    padding: theme.spacing(5),
    margin: theme.spacing(5),
  },
}));
