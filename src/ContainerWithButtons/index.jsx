import React from 'react';
import { Button, Paper, Grid } from '@material-ui/core';
import { CustomButton } from '../CustomButton';
import { useStyles } from './styles';

export const ContainerWithButtons = () => {
  const classes = useStyles();

  return (
    <Grid container justify="center">
      <Paper className={classes.root} square={false}>
        <Grid container spacing={4}>
          <Grid item xs={12} sm={6}>
            <Button variant="contained" color="secondary">
              Transparency
            </Button>
          </Grid>
          <Grid item xs={12} sm={6}>
            <CustomButton error />
          </Grid>
          <Grid item xs={12} sm={6}>
            <Button variant="contained">
              Default
            </Button>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Button variant="contained" color="primary">
              Primary
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </Grid>
  );
};
