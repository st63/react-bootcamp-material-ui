import React from 'react';
import { ThemeProvider } from '@material-ui/core/styles';
import { CssBaseline } from '@material-ui/core';
import { theme } from './themes/theme';
import { ContainerWithButtons } from './ContainerWithButtons';

function App() {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <ContainerWithButtons />
    </ThemeProvider>
  );
}

export default App;
